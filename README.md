# PRACTICA | MI PRIMERA APLICACION NATIVA

**Aplicando los conocimientos basicos obtenidos en clase vamos a desarrollar una aplicacion que nos** 
**muestre el clima en nuestro celular, la cual debera permitirnos implementar los fundamentos** 
**basicos de la navegacion simple, el consumo de web services y el mapeo de estos a objetos dentro** 
**de nuestra aplicacion.**

- *Jose Ricardo Avila Chavez (345635)*

## Pre-requisito
Tener instalado android studio

